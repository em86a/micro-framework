<?php
namespace Otzy\MicroFramework\Tests;

use Otzy\MicroFramework\Router;

class RouterTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function parseRequestURI()
    {
        //uri with query parameters
        $uri = '/abc/def/ghi/?a=b';
        $parsed_uri = Router::invokePrivate('parseRequestURI', [$uri, '/index.php']);
        $this->assertEquals(['abc', 'def', 'ghi'], $parsed_uri);

        //uri without query parameters
        $uri = '/abc/def/ghi';
        $parsed_uri = Router::invokePrivate('parseRequestURI', [$uri]);
        $this->assertEquals(['abc', 'def', 'ghi'], $parsed_uri);
    }

    /**
     * @test
     */
    public function setUpController()
    {

        $request_stub = $this->createMock(\Otzy\MicroFramework\RequestInterface::class);
        $request_stub->method('getRequestURI')->willReturn('/abc/def/ghi/?a=b');
        $router = Router::getInstance($request_stub, '\Otzy\MicroFramework\Controller');
        $router_success_message = $router->setUpController('Home');
        $this->assertContains('does not exist', $router_success_message);

        $request_stub = $this->createMock(\Otzy\MicroFramework\RequestInterface::class);
        $request_stub->method('getRequestURI')->willReturn('/Home/blabla/ghi/?a=b');
        $router->setRequest($request_stub);
        $router_success_message = $router->setUpController('Home');
        $this->assertContains('is not callable', $router_success_message);

        $request_stub = $this->createMock(\Otzy\MicroFramework\RequestInterface::class);
        $request_stub->method('getRequestURI')->willReturn('/Home/index');
        $router->setRequest($request_stub);
        $router_success_message = $router->setUpController('Home');
        $this->assertTrue($router_success_message);
        $this->assertInstanceOf(\Otzy\MicroFramework\Controllers\Home::class, $router->getController());
        $this->assertEquals('index', $router->getCurrentMethod());
        
    }


}