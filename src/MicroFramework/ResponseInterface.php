<?php

namespace Otzy\MicroFramework;


interface ResponseInterface
{
    /**
     * @param bool|string $message additional message to display in response body
     * @return void
     */
    public function respond404($message = false);

    public function redirect($code, $location);

    public static function getInstance();

    public function setCookie($name, $value = null, $expire = null, $path = null, $domain = null, $secure = null, $httponly = null);
}