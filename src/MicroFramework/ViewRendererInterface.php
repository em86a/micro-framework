<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 6/19/2016
 * Time: 9:17 PM
 */

namespace Otzy\MicroFramework;


interface ViewRendererInterface
{
    /**
     * @param $view_name
     * @param array $data
     * @param string[] $data_as_is Values that should be rendered without sanitizing
     */
    public function render($view_name, array $data = [], array $data_as_is = []);

    /**
     * @param string $templates_dir dir with theme
     * @return ViewRendererInterface
     */
    public static function getInstance($templates_dir);

    /**
     * @param $data
     * @return ViewRendererInterface
     */
    public function setGlobalData($data);
}