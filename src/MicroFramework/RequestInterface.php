<?php
namespace Otzy\MicroFramework;


interface RequestInterface
{
    /**
     * @return string
     */
    public function getHTTPMethod();

    /**
     * @return bool
     */
    public function isPost();

    /**
     * @return bool
     */
    public function isGet();

    /**
     * returns parameter from query, regardless where it was sent - in query string or in the body
     *
     * @param string $key
     * @param mixed $default_value
     * @return mixed
     */
    public function get($key, $default_value = false);

    /**
     * returns parameter from query string
     *
     * @param string $key
     * @param mixed $default_value
     * @return mixed
     */
    public function getFromGet($key, $default_value = false);

    /**
     * returns parameter from the request body. Not only POST but PUT too or whatever it was
     *
     * @param string $key
     * @param mixed $default_value
     * @return mixed
     */
    public function getFromPost($key, $default_value = false);

    /**
     * //TODO implement file structure interface
     *
     * @param string $key
     * @param mixed $default_value
     * @return mixed
     */
    public function getFile($key, $default_value = false);

    /**
     * @param string $key
     * @param mixed $default_value
     * @return mixed
     */
    public function getFromCookie($key, $default_value = false);

    //following functions normally are just wrappers of according $_SERVER elements
    //only getClientIP might be complex
    public function getRequestURI();

    public function getQueryString();

    public function getAcceptLanguages();

    public function getClientIP();

    public function getProtocol();

    public function getScriptName();
}