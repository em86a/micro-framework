<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 23.08.2016
 * Time: 23:42
 */

namespace Otzy\MicroFramework;
use Otzy\MicroFramework\Exceptions\CronException;


/**
 *  Common functions used by cronjobs
 */
class Cron
{

    public static $log_name = 'crons';

    public static $isParent = true; //for forked process management
    protected static $pids = array();

    private static $job_name = false;

    /**
     * @param string $job_name - mast be unique across all cron scripts
     * @param \Closure $callback
     * @param \Closure|bool $exception_callback must accept two parameters: subject and content
     * @param string $log_name
     * @throws \Exception
     */
    public static function runJob($job_name, $callback, $exception_callback = false, $log_name = 'crons')
    {
        if (self::$job_name !== false){
            throw new CronException('Function runJob() can be called only once');
        }

        self::$job_name = $job_name;

        if (function_exists('newrelic_background_job')){
            newrelic_background_job(true);
        }

        ignore_user_abort(true);

        session_write_close(); //we don't need session management in cron job. And it sucks with fork

        if (is_string($log_name)){
            self::$log_name = $log_name;
        }

        if (!Cron::cronLock($job_name)) {
            echo 'Script already running, execution stopped' . "\n";
            die;
        }

        // Call cronUnLock when the script is finished
        register_shutdown_function(array('Cron', 'cronUnLock'));

        if (!Cron::cronLog($job_name . ': started', true)) {
            echo 'ERROR: Cant access to cronjob log file, execution stopped: ' , $job_name , "\n";
            die;
        }

        try{

            call_user_func($callback);

        }catch(\Exception $e){
            if (is_callable($exception_callback)){
                call_user_func($exception_callback, 'Exception in '.self::$job_name, $e->getMessage() . "\n\n" . $e->getTraceAsString());
            }

            throw $e;
        }



        if (self::$isParent){//we don't write extra log records in children

            //wait until all children finished
            if (function_exists('pcntl_wait')){
                $status = 0;
                while(pcntl_wait($status) > 0){} //wait for all children finished
            }

            if (!Cron::cronLog($job_name . ': finished', true)) {
                echo 'ERROR: Cant access to cronjob log file, execution stopped: ' . $job_name . "\n";
                die;
            }
        }
    }

    protected static function getLogDir(){
        if (defined('LOG_DIR')){
            $log_dir = LOG_DIR;
        } else {
            $log_dir = sys_get_temp_dir() . '/logs/';
        }

        if (!file_exists($log_dir)){
            mkdir($log_dir, 0777, true);
            chmod($log_dir, 0777);
        }

        return $log_dir;
    }

    /**
     * Logging function for cronjobs. Will add the time and the end of line.
     *
     * @param string $text text that will be wrote in cronjob log file
     * @param bool $clioutput set if the same text wrote in the cronjob log file will be printed in the console
     * @return bool true on success
     */
    static function cronLog($text, $clioutput = false)
    {
         static $log_file = false;
        if ($log_file === false){
            $log_file = self::getLogDir() . self::$log_name. '_'.date('Ymd').'.log';
        }

        touch($log_file);
        @chmod($log_file, 0777);


        $text = date("Y-m-d H:i:s") . ' ' . $text . "\r\n";
        if (!@file_put_contents($log_file, $text, FILE_APPEND)) {
            error_log('ERROR: Cant write on cronjob log file "' . $log_file . '" the message { ' . $text . '}');
            return false;
        }

        if ($clioutput == true) {
            echo '<br> '.$text;
            ob_flush();flush();
        }
        return true;

    }

    /**
     * Checks/create a lock file to avoid a cronjob to run multiple times
     *
     * @param string $cronName name of the cronjob
     * @return bool false if lock file already exist true if the cronjob can be executed
     */
    static function cronLock($cronName)
    {
        $lock_file = self::getLogDir() . $cronName . '.lock';

        if (file_exists($lock_file)) {
            $modifydate = filemtime($lock_file);
            $max_time = 60 * 30; # 30 minutes

            // If file exist the cronjob its already running
            if (($modifydate + $max_time > time())) {
                $log_msg = $cronName . ': ERROR: lock file detected: ' . realpath(
                        $lock_file
                    ) . '. Cronjob alredy running: ' . $cronName . '; execution stopped.' . "\r\n";
                self::cronLog($log_msg);
                return false;
            }

        }
        // lets make a lock file
        if (!@touch($lock_file)) {
            $log_msg = $cronName . ': ERROR: unable to create a lock file in: ' .  $lock_file
                . ' cronjob execution aborted.' . "\r\n";
            error_log($log_msg);
            self::cronLog($log_msg);
            return false;
        }

        return true;
    }

    /**
     *  Delete a lock file
     *
     * @param string $cronName name of the cronjob
     * @return bool true on success false on error
     */
    static function cronUnLock($cronName)
    {
        //only parent process can unlock
        if (!self::$isParent){
            return true;
        }

        //send email to errors in case of fatal error
        $error = error_get_last();
        if ($error !== null) {
            if (!($error['type'] & (E_WARNING + E_STRICT + E_NOTICE))) {
                mail('errors@qipu.de', 'Cron Job ' . $cronName . ' error', var_export($error, true));
                var_dump($error);
            }
        }


        $lock_file = self::getLogDir() . $cronName . '.lock';

        // Delete lock file
        if (@unlink($lock_file)) {
            return true;
        }
        if (file_exists($lock_file)) {
            $log_msg = $cronName . ' ERROR: lock file exists but unable to delete it in: ' . realpath(
                    $lock_file
                ) . '  cronjob would be unable to initialize next time since the lock file exists' . "\n";
            self::cronLog($log_msg);
            return false;
        }
        $log_msg = $cronName . ' WARNING: unable to delete the lock file in: ' . realpath(
                $lock_file
            ) . ' (file not exists)' . "\n";
        self::cronLog($log_msg);
        return false;
    }


    //The following two functions use lock file as a status storage for the currently running job
    /**
     * @param string $cronName
     * @param string $progress_value any string
     * @return bool
     */
    public static function setProgress($cronName, $progress_value){
        $lock_file = self::getLogDir() . $cronName . '.lock';
        if (!@touch($lock_file)){
            return false;
        }else{
            return file_put_contents($lock_file, $progress_value) !== false;
        }
    }

    /**
     * @param $cronName
     * @return bool|string
     */
    public static function getProgress($cronName){
        $lock_file = self::getLogDir() . $cronName . '.lock';
        if (file_exists($lock_file)){
            return file_get_contents($lock_file);
        }else{
            return false;
        }
    }

    public static function fork()
    {

        //only parent can fork
        if (!self::$isParent){
            return;
        }

        $pid = pcntl_fork();
        if ($pid == -1) {
            throw new CronException('Fork Error');
        }

        if ($pid > 0) {
            //this is parent process
            self::$pids[] = $pid;
        } else {
            self::$isParent = false;
        }
    }

    public static function waitChildren(){
        if (self::$isParent){

            //wait until all children finished
            if (function_exists('pcntl_wait')){
                $status = 0;
                while(pcntl_wait($status) > 0){
                    usleep(100);
                } //wait for all children finished
            }
        }
    }

}