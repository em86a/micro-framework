<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 6/19/2016
 * Time: 9:18 PM
 */

namespace Otzy\MicroFramework;


use Otzy\MicroFramework\Exceptions\NotImplementedException;

class TwigViewRenderer implements ViewRendererInterface
{
    /**
     * Data, available in templates by default
     *
     * @var array
     */
    protected $global_data = [];

    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * @var \Twig_LoaderInterface
     */
    protected $loader;

    public function __construct($templates_dir)
    {
        $this->loader = new \Twig_Loader_Filesystem($templates_dir);
    }

    public function setEnvironment($environment = []){
        $this->twig = new \Twig_Environment($this->loader, $environment);

        if (isset($environment['debug']) && $environment['debug'] === true){
            $this->twig->addExtension(new \Twig_Extension_Debug());
        }
    }

    public function setGlobalData($data)
    {
        $this->global_data = array_merge($this->global_data, $data);
        return $this;
    }

    /**
     * @param $templates_dir
     * @return static
     */
    public static function getInstance($templates_dir)
    {
        static $instance;
        if (!($instance instanceof ViewRendererInterface)) {
            $instance = new static($templates_dir);
        }

        return $instance;
    }

    /**
     * if we need to change theme in controller for some reason
     *
     * @param $templates_dir
     * @throws
     */
    public function setTemplatesDir(/** @noinspection PhpUnusedParameterInspection */
        $templates_dir)
    {
        throw new NotImplementedException(__METHOD__ . ' is not implemented'); 
    }

    /**
     * @param $view_name
     * @param array $data
     * @param string[] $data_as_is Values that should be rendered without sanitizing
     */
    public function render($view_name, array $data = [], array $data_as_is = [])
    {
        //Clean up things
        $this->sanitize($this->global_data);
        $this->sanitize($data);
        //////////////////

        $this->twig->display($view_name . '.html', array_merge($this->global_data, $data, $data_as_is));
    }

    protected function sanitize(array &$data)
    {
        foreach ($data as &$value) {
            if (is_string($value)) {
                $value = strip_tags($value);
            }
        }
        unset($value);
    }
}