<?php
namespace Otzy\MicroFramework;


class Router implements RouterInterface
{
    use TestableTrait;

    /**
     * Class name of controller including namespace, all controllers must inherit from
     * @var string
     */
    protected $controllers_parent;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var string
     */
    protected $controllers_namespace = '\App\Controllers';

    /**
     * @var ControllerInterface
     */
    protected $current_controller;

    /**
     * @var string
     */
    protected $current_method;

    /**
     * @var array;
     */
    protected $current_params;

    /**
     * Router constructor.
     * @param RequestInterface $request
     */
    private function __construct(RequestInterface $request)
    {
        $this->request = $request;
    }

    public function setRequest(RequestInterface $request)
    {
        $this->request = $request;
    }

    /**
     * Returns single instance of our Router
     *
     * @param RequestInterface $request
     * @return static
     * @throws
     */
    public static function getInstance(RequestInterface $request)
    {
        static $instance;
        if (!($instance instanceof Router)) {
            $instance = new static($request);
        }

        return $instance;
    }

    /**
     * Namespace where all controllers must be defined
     * Implementation may have default namespace defined,
     * so it might be not necessarily to call this function in the app bootstrap
     *
     * @param $namespace
     * @return mixed
     */
    public function setControllersNamespace($namespace)
    {
        $this->controllers_namespace = $namespace;
        return true;
    }

    /**
     * runs controller previously defined with setController
     *
     * @return void
     */
    public function dispatch()
    {
        $this->current_controller->runMethod($this->current_method, $this->current_params);
    }

    /**
     * parses request parameters and set up controller, method and parameters, passed to the method
     *
     * Our router is simple.
     * Query always has the same structure. GET and POST are processed alike.
     * It's a responsibility of controller to take care of HTTP method
     *
     * Paths are relative to the path of index.php
     *
     * /controller/method/[param1[/param2[/param2...]]]
     *
     * returns true if requested successfully parsed and controller's method is callable
     * If something is wrong, text string with info for debugging is returned
     *
     * @param string $default_controller
     * @param string $home_path
     * @return bool|string
     */
    public function setUpController($default_controller, $home_path = '/')
    {
        $uri_parts = $this->parseRequestURI($this->request->getRequestURI(), $home_path);

        if (count($uri_parts) < 1 || $uri_parts[0] == '') {
            //this is a root path
            $controller_name = $default_controller;
        } else {
            $controller_name = $uri_parts[0];
        }

        //run controller in defined controllers namespace.
        //We don't want to give an opportunity to run arbitrary function
        $controller_class_name = $this->controllers_namespace . '\\' . $controller_name;

        if (!class_exists($controller_class_name, true)) {
            return 'Class ' . $controller_class_name . ' does not exist.';
        }

        //retrieve method and check that it's public
        if (count($uri_parts) < 2) {
            $this->current_method = false;
        } else {
            $this->current_method = $uri_parts[1];
        }

        /* @var ControllerInterface $controller */
        $this->current_controller = new $controller_class_name();

        //We want only public methods. For performance reason don't use reflection
        if (is_string($this->current_method) && !is_callable([$this->current_controller, $this->current_method])) {
            return 'method ' . $this->current_method . ' of class ' . $controller_class_name . ' is not callable';
        }

        //all further elements of URI we treat as parameters of the called method
        if (count($uri_parts) > 2) {
            $this->current_params = array_slice($uri_parts, 2);
        } else {
            $this->current_params = [];
        }

        return true;
    }

    /**
     * @param string $request_uri
     * @param string $home_path
     * @return string[]
     */
    protected static function parseRequestURI($request_uri, $home_path)
    {
        $request_uri = urldecode($request_uri);

        //get read of query string part
        $request_uri = preg_replace('/\?.*$/', '', $request_uri);

        //we don't need / in the end and in the beginning as well
        $request_uri = preg_replace('/^\\'.$home_path.'\/*|\/$/', '', $request_uri);

        return explode('/', $request_uri);
    }

    public function getController()
    {
        return $this->current_controller;
    }

    /**
     * @return string
     */
    public function getCurrentMethod()
    {
        return $this->current_method;
    }
}