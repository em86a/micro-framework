<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 6/19/2016
 * Time: 1:16 AM
 */

namespace Otzy\MicroFramework;

interface RouterInterface
{
    /**
     * @param RequestInterface $request
     * @return RouterInterface
     */
    public static function getInstance(RequestInterface $request);

    /**
     * runs controller based on request parameters
     *
     * @return mixed
     */
    public function dispatch();

}