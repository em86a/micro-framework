<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 6/26/2016
 * Time: 9:58 PM
 */

namespace Otzy\MicroFramework;


trait TestableTrait
{
    /**
     * @param string $method_name
     * @param array $arguments
     * @return mixed
     *
     * @codeCoverageIgnore
     */
    public static function invokePrivate($method_name, $arguments)
    {
        $method = (new \ReflectionClass(static::class))->getMethod($method_name);
        $method->setAccessible(true);
        return $method->invokeArgs(null, $arguments);
    }
}