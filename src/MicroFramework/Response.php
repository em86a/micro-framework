<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 6/19/2016
 * Time: 10:32 PM
 */

namespace Otzy\MicroFramework;


class Response implements ResponseInterface
{
    private function __construct()
    {
    }

    public static function getInstance()
    {
        static $instance;
        if (!($instance instanceof ResponseInterface)) {
            $instance = new static();
        }

        return $instance;
    }

    public static $http_codes = [
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => 'Switch Proxy',
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect',
    ];

    /**
     * @param string|bool $message additional message to display in response body
     */
    public function respond404($message = false)
    {
        if (!headers_sent()) {
            header('HTTP/1.1 404 Not Found');
        }
        if (is_string($message)){
            echo $message;
        }

        die;
    }

    public function redirect($code, $location)
    {
        $code = intval($code);
        if ($code < 300 || $code > 308) {
            throw new \InvalidArgumentException('Invalid redirect HTTP code.');
        }
        header("HTTP/1.1 $code " . self::$http_codes[$code]);
        header('Location: ' . $location);
        die;
    }

    /**
     * just wrapper for php setcookie
     * 
     * @param $name
     * @param null $value
     * @param null $expire
     * @param null $path
     * @param null $domain
     * @param null $secure
     * @param null $httponly
     */
    public function setCookie($name, $value = null, $expire = null, $path = null, $domain = null, $secure = null, $httponly = null) {
        setcookie($name, $value, $expire, $path, $domain, $secure, $httponly);
     }
}