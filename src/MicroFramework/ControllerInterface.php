<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 6/19/2016
 * Time: 8:45 PM
 */

namespace Otzy\MicroFramework;


interface ControllerInterface
{
    public function __construct(RequestInterface $request = null, ResponseInterface $response = null,
                                ViewRendererInterface $view_renderer = null);

    /**
     * @param ViewRendererInterface $view_renderer
     * @return ControllerInterface
     */
    public function setViewRenderer(ViewRendererInterface $view_renderer);

    /**
     * @param RequestInterface $request
     * @return ControllerInterface
     */
    public function setRequest(RequestInterface $request);

    /**
     * @param ResponseInterface $response
     * @return ControllerInterface
     */
    public function setResponse(ResponseInterface $response);

    public function runMethod($method_name, array $params);

    /**
     * Renders desired view
     *
     * @param string $view_name
     * @param array $data
     * @return void
     */
    public function render($view_name, array $data);
}