<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 19.08.2016
 * Time: 21:56
 */

namespace Otzy\MicroFramework;


class Configuration{
    const ENV_DEV = 0;
    const ENV_ADMIN = 1;
    const ENV_PROD = 2;

    /**
     * MUST BE SET in application bootstrap
     *
     * @var int
     */
    protected static $environment = 2;

    /**
     * @param int $env 0 - dev, 1 - admin, 2 - production
     */
    public static function setEnv($env){
        static::$environment = $env;
    }

    public static function isDev(){
        return static::$environment === self::ENV_DEV;
    }

    public static function isAdmin(){
        return static::$environment === self::ENV_ADMIN;
    }

    public static function isProduction(){
        return static::$environment === self::ENV_PROD;
    }
}