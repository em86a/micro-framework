<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 6/19/2016
 * Time: 6:52 PM
 */

namespace Otzy\MicroFramework;

abstract class Controller implements ControllerInterface
{
    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var ResponseInterface
     */
    protected $response;

    /**
     * @var ViewRendererInterface|PHPViewRenderer
     */
    protected $view_renderer;

    public function __construct(RequestInterface $request = null, ResponseInterface $response = null,
                                ViewRendererInterface $view_renderer = null)
    {
        $this->request = $request;
        $this->response = $response;
        $this->view_renderer = $view_renderer;
    }

    public function setRequest(RequestInterface $request)
    {
        $this->request = $request;
        return $this;
    }

    public function setResponse(ResponseInterface $response)
    {
        $this->response = $response;
        return $this;
    }

    public function setViewRenderer(ViewRendererInterface $view_renderer)
    {
        $this->view_renderer = $view_renderer;
        return $this;
    }

    final public function runMethod($method_name, array $params)
    {
        if (!is_string($method_name)) {
            $method_name = $this->getDefaultMethod();
        }

        call_user_func_array([$this, $method_name], $params);
    }

    public function render($view_name, array $data)
    {
        $this->view_renderer->setGlobalData($this->prepareGlobalData())
            ->render($view_name, $data);
    }

    /**
     * data passed to every view
     */
    protected function prepareGlobalData()
    {
        $global_data = ['error_message' => false, 'success_message' => false];

        if (isset($_SESSION['error_message'])) {
            $global_data['error_message'] = $_SESSION['error_message'];
        }

        if (isset($_SESSION['success_message'])) {
            $global_data['success_message'] = $_SESSION['success_message'];
        }

        $this->clearSessionMessages(); //we want to display them only once

        return $global_data;
    }

    public function clearSessionMessages()
    {
        unset($_SESSION['error_message']);
        unset($_SESSION['success_message']);
    }

    public function setSessionErrorMessage($message)
    {
        $_SESSION['error_message'] = $message;
    }

    public function setSessionSuccessMessage($message)
    {
        $_SESSION['success_message'] = $message;
    }

    /**
     * the name of method that must be called when no method passed in URL, i.e. only controller name
     *
     * @return string
     */
    abstract protected function getDefaultMethod();
}