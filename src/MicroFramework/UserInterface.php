<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 19.08.2016
 * Time: 22:12
 */

namespace Otzy\MicroFramework;


interface UserInterface{
    /**
     * @return int
     */
    public function getUserId();

    /**
     * checks that the password is correct and matches the stored hash
     *
     * @param $password
     * @return bool
     */
    public function checkPassword($password);

    /**
     * sets a new password
     * @param string $password
     * @return bool
     */
    public function updatePassword($password);
}