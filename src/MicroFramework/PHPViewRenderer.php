<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 6/19/2016
 * Time: 9:18 PM
 */

namespace Otzy\MicroFramework;


class PHPViewRenderer implements ViewRendererInterface
{
    /**
     * @var string
     */
    protected $templates_dir;

    /**
     * Data, available in templates by default
     *
     * @var array
     */
    protected $global_data = [];

    public function __construct($templates_dir)
    {
        $this->templates_dir = $templates_dir;
    }

    public function setGlobalData($data)
    {
        $this->global_data = array_merge($this->global_data, $data);
        return $this;
    }

    /**
     * @param $templates_dir
     * @return static
     */
    public static function getInstance($templates_dir)
    {
        static $instance;
        if (!($instance instanceof ViewRendererInterface)) {
            $instance = new static($templates_dir);
        }

        return $instance;
    }

    /**
     * if we need to change theme in controller for some reason
     *
     * @param $templates_dir
     */
    public function setTemplatesDir($templates_dir)
    {
        $this->templates_dir = $templates_dir;
    }

    /**
     * @param $view_name
     * @param array $data
     * @param string[] $data_as_is Values that should be rendered without sanitizing
     */
    public function render($view_name, array $data = [], array $data_as_is = [])
    {
        $view_file_name = $this->templates_dir . $view_name . '.tpl.php';
        if (!file_exists($view_file_name)) {
            throw new \InvalidArgumentException('View ' . $view_name . ' does not exists in ' . $this->templates_dir);
        }

        //Clean up things
        $this->sanitize($this->global_data);
        $this->sanitize($data);
        //////////////////

        //Make variables from arrays
        extract($this->global_data); //global data has lower priority and can be overwritten by $data
        extract($data);
        extract($data_as_is);

        /** @noinspection PhpIncludeInspection */
        include($view_file_name);
    }

    protected function sanitize(array &$data)
    {
        foreach ($data as &$value) {
            if (is_string($value)) {
                $value = strip_tags($value);
            }
        }
        unset($value);
    }
}