<?php
namespace Otzy\MicroFramework;

use Otzy\MicroFramework\Exceptions\UnexpectedException;

class Auth
{
    /**
     * @var Auth
     */
    private static $user_instance;


    /**
     * @var Auth
     */
    private static $admin_instance;

    /**
     * @var AuthSessionInterface
     */
    private $auth_session;

    /**
     * @var UserStorageInterface
     */
    private $user_storage;

    private function __construct(AuthSessionInterface $auth_session, UserStorageInterface $user_storage)
    {
        $this->auth_session = $auth_session;
        $this->user_storage = $user_storage;
    }

    /**
     * creates and return user auth object
     *
     * @param AuthSessionInterface $auth_session
     * @param UserStorageInterface $user_storage
     * @return \Otzy\MicroFramework\Auth
     */
    public static function createInstance(AuthSessionInterface $auth_session, UserStorageInterface $user_storage)
    {
        if (!(self::$user_instance instanceof Auth)) {
            self::$user_instance = new self($auth_session, $user_storage);
        }

        return self::$user_instance;
    }

    /**
     * returns user auth object
     *
     * @return Auth
     * @throws \Exception
     */
    public static function getInstance()
    {
        if (self::$user_instance instanceof Auth) {
            return self::$user_instance;
        }

        throw new \Exception('You should createInstance first.');
    }

    /**
     * creates and returns admin_user auth object
     *
     * @param AuthSessionInterface $auth_session
     * @param UserStorageInterface $admin_user_storage
     * @return \Otzy\MicroFramework\Auth
     */
    public static function createAdminInstance(AuthSessionInterface $auth_session, UserStorageInterface $admin_user_storage)
    {
        if (!(self::$admin_instance instanceof Auth)) {
            self::$admin_instance = new self($auth_session, $admin_user_storage);
        }

        return self::$admin_instance;
    }

    /**
     * returns admin_user user auth object
     *
     * @return Auth
     * @throws \Exception
     */
    public static function getAdminInstance()
    {
        if (self::$admin_instance instanceof Auth) {
            return self::$admin_instance;
        }

        throw new \Exception('You should createInstance first.');
    }

    /**
     * forces login by username without password check
     *
     * @param int $user_id
     * @param bool $remember_me
     * @return UserInterface|bool
     * @throws
     */
    public function loginByUserId($user_id, $remember_me = false){
        try {

            $user = $this->user_storage->getUserById($user_id);

            if ($user instanceof UserInterface) {
                $this->auth_session->createSession($user_id, $remember_me);
                return $user;
            }

            return false;

        } catch (\Exception $e) {
            if (Configuration::isProduction()) {
                throw new UnexpectedException('something nasty happened.');
            } else {
                throw $e;
            }
        }
    }

    /**
     * forces login by username without password check
     *
     * @param $username
     * @param bool $remember_me
     * @return UserInterface|bool
     * @throws
     */
    public function loginByUsername($username, $remember_me = false){
        try {
            $user = $this->user_storage->getUserByUsername($username);

            if ($user instanceof UserInterface) {
                $this->auth_session->createSession($user->getUserId(), $remember_me);
                return $user;
            }

            return false;

        } catch (\Exception $e) {
            if (Configuration::isProduction()) {
                throw new UnexpectedException('something nasty happened.');
            } else {
                throw $e;
            }
        }
    }

    /**
     * @param string $username
     * @param string $password
     * @param bool $remember_me if true creates session with longer expiration date
     * @return bool|UserInterface
     * @throws
     */
    public function loginByPassword($username, $password, $remember_me = false)
    {
        try {
            $user = $this->user_storage->getUserByUsername($username);

            if (!($user instanceof UserInterface)) {
                return false;
            }

            if ($user->checkPassword($password)){
                $this->auth_session->createSession($user->getUserId(), $remember_me);
                return $user;
            }

            return false;

        } catch (\Exception $e) {
            if (Configuration::isProduction()) {
                throw new UnexpectedException('something nasty happened.');
            } else {
                throw $e;
            }
        }
    }

    /**
     * @return mixed
     */
    public function logout()
    {
        return $this->auth_session->expireCurrentSession();
    }

}