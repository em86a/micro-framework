<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 19.08.2016
 * Time: 21:28
 */

namespace Otzy\MicroFramework;

/**
 * Interface AuthSessionInterface
 * @package Otzy\MicroFramework
 *
 * when bootstrapping your application, you should create an empty instance of your AuthSession class in order to
 * provide an access to static function of the AuthSessionInterface in Auth class
 *
 */
interface AuthSessionInterface{

    /**
     * Here you should take care of processing session cookie and retrieving user_id
     *  and other session parameters (initial ip address, country, ... - whatever you need in your application)
     *
     * @return AuthSessionInterface
     */
    public static function getInstance();

    /**
     * returns user_id if session cookie contained not expired sessionid, false otherwise
     *
     * @return bool|int
     */
    public function getUserId();

    /**
     * Expire current session (based on session cookie)
     *
     * @return bool
     */
    public function expireCurrentSession();

    /**
     * deletes authentication cookie and expire session in session storage
     * performs log out
     *
     * @param $session_token
     * @return mixed
     */
    public function expireByToken($session_token);

    /**
     * expire all session of given user
     *
     * @param int $user_id
     * @return bool
     */
    public function expireAllSessionsOfUser($user_id);

    /**
     * expire all sessions of all users
     *
     * @return bool
     */
    public function expireAllSessions();

    /**
     * performs log in
     * creates session and sets session cookie
     *
     * @param int $user_id
     * @param bool $remember_me if true, expiration time is much longer than usually
     * @return bool
     */
    public function createSession($user_id, $remember_me = false);

    /**
     * update the last access time and increase expiration time
     * basically you should call this function right after getInstance() if you want to extend session lifetime after each time user interacts with your application
     *
     * @return bool
     */
    public function touch();
}