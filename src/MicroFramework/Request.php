<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 6/19/2016
 * Time: 6:15 PM
 */

namespace Otzy\MicroFramework;

use Otzy\MicroFramework\Exceptions;


class Request implements RequestInterface
{
    protected $get_data = [];
    protected $post_data = [];
    protected  $cookie_data = [];

    protected function __construct()
    {
        $this->get_data = $_GET;
        $this->post_data = $_POST;
        $this->cookie_data = $_COOKIE;
    }

    public static function getInstance()
    {
        static $instance;

        if (!($instance instanceof RequestInterface)) {
            $instance = new static();
        }

        return $instance;
    }

    public function clean(){
        foreach ($this->get_data as $k => $v) {
            $this->cleanElement($this->get_data[$k]);
        }

        foreach ($this->post_data as $k => $v) {
            $this->cleanElement($this->post_data[$k]);
        }

        foreach ($this->cookie_data as $k => $v) {
            $this->cleanElement($this->cookie_data[$k]);
        }
    }

    private function cleanElement(&$el){
        if (is_array($el)){
            foreach ($el as $k=>$v){
                $this->cleanElement($v);
                $el[$k] = $v;
            }
            return;
        }

        $el = trim(htmlentities(strip_tags(strval($el))));
    }

    public function getHTTPMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    /**
     * @return bool
     */
    public function isPost()
    {
        return strtolower($this->getHTTPMethod()) == 'post';
    }

    /**
     * @return bool
     */
    public function isGet()
    {
        return strtolower($this->getHTTPMethod()) == 'get';
    }

    /**
     * returns parameter from query, regardless where it was sent - in query string or in the body
     *
     * if parameter exists in query string and body, parameter from query string returned
     *
     * @param string $key
     * @param mixed $default_value
     * @return mixed
     */
    public function get($key, $default_value = false)
    {
        //Note: intentionally repeated myself for performance and code simplicity reasons

        if (isset($this->get_data[$key])) {
            return $this->get_data[$key];
        }

        if (isset($this->post_data[$key])) {
            return $this->post_data[$key];
        }

        return $default_value;
    }

    /**
     * returns parameter from query string
     *
     * @param string $key
     * @param mixed $default_value
     * @return mixed
     */
    public function getFromGet($key, $default_value = false)
    {
        if (isset($this->get_data[$key])) {
            return $this->get_data[$key];
        }

        return $default_value;
    }

    /**
     * returns parameter from the request body. Not only POST but PUT too or whatever it was
     *
     * @param string $key
     * @param mixed $default_value
     * @return mixed
     */
    public function getFromPost($key, $default_value = false)
    {
        if (isset($this->post_data[$key])) {
            return $this->post_data[$key];
        }

        return $default_value;
    }

    /**
     * @param string $key
     * @param mixed $default_value
     * @return mixed
     */
    public function getFromCookie($key, $default_value = false) {
        if (isset($this->cookie_data[$key])) {
            return $this->cookie_data[$key];
        }

        return $default_value;
    }

    /**
     * //TODO implement uploaded file structure interface
     *
     * @param string $key
     * @param mixed $default
     * @return mixed
     * @throws
     */
    public function getFile($key, $default = false)
    {
        throw new Exceptions\NotImplementedException(__METHOD__ . ' ' . 'key=' . $key);
    }

    //following functions are basically wrappers of according $_SERVER members
    //only getClientIP and getProtocol might be complex
    public function getRequestURI()
    {
        return $_SERVER['REQUEST_URI'];
    }

    public function getQueryString()
    {
        return $_SERVER['QUERY_STRING'];
    }

    public function getAcceptLanguages()
    {
        return $_SERVER['HTTP_ACCEPT_LANGUAGE'];
    }

    public function getClientIP()
    {
        throw new Exceptions\NotImplementedException(__METHOD__);
    }

    public function getProtocol()
    {
        throw new Exceptions\NotImplementedException(__METHOD__);
    }

    public function getScriptName()
    {
        return $_SERVER['SCRIPT_NAME'];
    }
}