<?php

namespace Otzy\MicroFramework;


interface UserStorageInterface{
    /**
     * @param $username
     * @return UserInterface|bool - user or false is user is not found
     */
    public function getUserByUsername($username);

    /**
     * @param int $id
     * @return UserInterface|bool
     */
    public function getUserById($id);

    /**
     * @param string $username
     * @return bool
     */
    public function ifUserExists($username);

    /**
     * @param string $username
     * @param string $password
     * @return bool|UserInterface
     */
    public function addUser($username, $password);

}